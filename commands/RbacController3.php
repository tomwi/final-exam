<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{


	

	public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$dealAct = $auth->createPermission('dealAct');
		$dealAct->description = 'team leaders and above can view, edit and create deals ';
		$auth->add($dealAct);	
	}
	
	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
			
		
		$teamleader = $auth->getRole('teamleader');
		$auth->addChild($teamleader, $teammember);
		
		$dealAct = $auth->getPermission('dealAct');
		$auth->addChild($teamleader, $dealAct);

		
	}
}