<?php

namespace app\models;
use yii\helpers\ArrayHelper;	
use Yii;
use app\models\Lead;


/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'name', 'amount'], 'required'],
            [['id', 'leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead id',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
		public static function getamount()
	{
		$amounts = ArrayHelper::
					map(self::find()->all(), 'id', 'amount');
		return $amounts;						
	}
	
	public static function getamountswithallamounts()
	{
		$amounts = self::getamount();
		$amounts[-1] = 'All amounts';
		$amounts = array_reverse ( $amounts, true );
		return $amounts;	
	
}
public static function getleads()
	{
		$leads = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $leads;						
	}
	
	public static function getleadswithallleads()
	{
		$leads = self::getleads();
		$leads[-1] = 'All leads';
		$leads = array_reverse ( $leads, true );
		return $leads;	
	}
	
}
