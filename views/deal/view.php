<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Deal;
use app\models\Lead;
use app\models\user;

/* @var $this yii\web\View */
/* @var $model app\models\Deal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Deals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'leadId',
            'name',
            'amount',
			
			
			
			//	[ // the status name 
			//	'label' => $model->attributeLabels()['leadId'],
			//	'value' => $model->leadname->name,	
		//	],			
			
		// 		[ // the owner name of the lead
			//	'label' => $model->attributeLabels()['leadId'],
			//	'format' => 'html',
			//	'value' => Html::a($model->leads->name, 
					//['deal/view', 'id' => $model->leads->id]),	
			//],
        ],
    ]) ?>

</div>
