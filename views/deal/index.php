<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lead;	
use yii\helpers\ArrayHelper;	
use app\models\Deal;	

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'leadId',
            'name',
           // 'amount',
			
		[
				'attribute' => 'amount',
				'label' => 'amount',
				'format' => 'raw',
				'value' => function($model){
					return $model->amount->amount;
				},
				'filter'=>Html::dropDownList('DealSearch[amount]', $amount, $amounts, ['class'=>'form-control']),
		],		
[  
			'attribute' => 'leadId',
				'label' => 'lead name',
				'format' => 'raw',
				'value' => function($model){
					return $model->leads->name;
				},
				'filter'=>Html::dropDownList('DealSearch[leadId]', $lead, $leads, ['class'=>'form-control']),
			],	
			//Html::activeDropDownList($model, 'lead',
					//ArrayHelper::map(Lead::find()->all(), 'id', 'name')) 	,	

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
